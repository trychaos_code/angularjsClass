commonContainerModule.config(function($routeProvider) {
    $routeProvider


        // route for the about page
        .when('/game', {
            templateUrl : 'js/template/game.html',
            controller  : 'gameController'
        })
        .when('/calc', {
            templateUrl : 'js/template/calculator.html',
            controller  : 'calculatorController'
        })
        .when('/emp', {
            templateUrl : 'js/template/employee.html',
            controller  : 'employeeController'
        })
        .otherwise({ redirectTo: '/emp' });
});

commonContainerModule.controller('gameController', function ($scope) {
        console.log("I am in gameController");
});
commonContainerModule.controller('employeeController', function ($scope, $http, $timeout) {
    console.log("I am in employeeController");
    $scope.employeeList = [];
    $scope.showModel = false;
    $scope.tempEmployee = {};

    $scope.addEmployee = function () {
        var data = $scope.tempEmployee;
        console.log(data);

        $http.post('http://localhost:3201/add-employee', {employee: data}).then(function (data) {
            $scope.employeeList = data.data;
            $scope.tempEmployee = {};
            $scope.showModel = false;
        })
    };

    $timeout(function () {
        $http.get('http://localhost:3201/employee-list').then(function (data) {
            $scope.employeeList = data.data;
        });
    }, 100);
});
commonContainerModule.controller('calculatorController', function ($scope) {
    console.log("I am in calculatorController");
});
