var commonContainerModule = angular.module("sakethModule",['ngRoute']);

commonContainerModule.directive('mainPage',function(){
    return {
        restrict:'EA',
        templateUrl: 'js/template/main-page.html'
    };
});
